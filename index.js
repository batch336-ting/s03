/* Quiz 1:
1. What is the blueprint where objects are created from?
2. What is the naming convention applied to classes?
3. What keyword do we use to create objects from a class?
4. What is the technical term for creating an object from a class?
5. What class method dictates HOW objects will be created from that class? 

Answers:

1. Class
2. PascalCase or CamelCase
3. new
4. Instantiation
5. Constructor method */

/* Quiz 2:
1. Should class methods be included in the class constructor?
2. Can class methods be separated by commas?
3. Can we update an object’s properties via dot notation?
4. What do you call the methods used to regulate access to an object’s properties?
5. What does a method need to return in order for it to be chainable?

Answers:
1. No
2. Yes
3. Yes
4. getters and setters
5. this */

// Function Code

class Student{
	constructor(name, email, grades){
		this.name = name;
		this.email = email;
		if(grades.length === 4){
			if(grades.every(grade => {return grade >= 0 && grade <= 100})){
				this.grades = grades
			}
		} else{
			this.grades = undefined;
		}

		this.gradeAve = undefined;
		this.isPass = undefined;
		this.isWithHonors = undefined;
	}
	login() {
	    console.log(`${this.email} has logged in`);
	    return this
	}

	logout() {
	    console.log(`${this.email} has logged out`);
	    return this
	}

	listGrades() {
	    console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
	    return this
	}
	computeAve() {
	    let sum = 0;
	    this.grades.forEach(grade => sum = sum + grade);
	    this.gradeAve = sum / 4;
	    return this;
	}

	willPass() {
	    this.isPass = this.computeAve() >= 85 ? true : false;
	    return this;
	}

	willPassWithHonors() {
	    this.isWithHonors = (this.willPass() && this.computeAve() >= 90) ? true : false;
	    return this;
	}
}

// Instantiate students
const studentOne = new Student('John', 'john@mail.com', [90, 88, 92, 86]);
const studentTwo = new Student('Joe', 'joe@mail.com', [78, 85, 88, 90]);
const studentThree = new Student('Jane', 'jane@mail.com', [95, 92, 89, 87]);
const studentFour = new Student('Jessie', 'jessie@mail.com', [80, 82, 78, 88]);



